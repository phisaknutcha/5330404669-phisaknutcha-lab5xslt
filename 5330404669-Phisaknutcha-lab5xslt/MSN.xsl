<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <head>
			    <title>MSN Log</title>            
            </head>
  <body>
    <text Style="font-family:Segoe UI; color:#FF0000; ">
		[Conversation started on 
			<xsl:value-of select="Log/Message/@Date"/> <xsl:text> </xsl:text>
			<xsl:value-of select="Log/Message/@Time"/> 
		]
	</text>
		
			<tr>
                    <td>
						<text Style="font-family:Segoe UI; color:#000000">
							[
							<xsl:text> </xsl:text>
							<xsl:value-of select="Log/Message[1]/@Date"/> <xsl:text> </xsl:text>
							<xsl:value-of select="Log/Message[1]/@Time"/>
							]
						</text>
                    </td>
                    <td>
                        <text Style="font-family:Segoe UI; color:#FFAA00">
                            <xsl:value-of select="Log/Message[1]/From/User/@FriendlyName"/>   
						</text>
                    </td>
					<td> 
						: 
					</td>
                    <td>
						<text Style="font-family:Segoe UI; color:#FFAA00">
                        <xsl:value-of select="Log/Message[1]/Text"/>
                        </text>		
                    </td>
                </tr>
                <tr>
                    <td>
						<text Style="font-family:Segoe UI; color:#000000">
							[	
								<xsl:text> </xsl:text>
								<xsl:value-of select="Log/Message[2]/@Date"/> <xsl:text> </xsl:text>
								<xsl:value-of select="Log/Message[2]/@Time"/>
							]
						</text>
                    </td>
                    <td>
                        <text Style="font-family:Segoe UI; color:#24913C">
                            <xsl:value-of select="Log/Message[2]/From/User/@FriendlyName"/>
						</text>	
                    </td>
					<td> 
						: 
					</td>
                    <td>
                        <text Style="font-family:Segoe UI; color:#24913C">
                            <xsl:value-of select="Log/Message[2]/Text"/>
                        </text>	
                    </td>
                </tr>
                <tr>
                    <td>
						<text Style="font-family:Segoe UI; color:#000000">
							[
							<xsl:text> </xsl:text>
							<xsl:value-of select="Log/Message[3]/@Date"/> <xsl:text> </xsl:text>
							<xsl:value-of select="Log/Message[3]/@Time"/>
							]
						</text>
                    </td>
                    <td>
                        <text Style="font-family:Segoe UI; color:#FFAA00">
                            <xsl:value-of select="Log/Message[3]/From/User/@FriendlyName"/>   
						</text>
                    </td>
					<td> 
						: 
					</td>
                    <td>
						<text Style="font-family:Segoe UI; color:#FFAA00">
                        <xsl:value-of select="Log/Message[3]/Text"/>
                        </text>		
                    </td>
                </tr>
                <tr>
                    <td>
						<text Style="font-family:Segoe UI; color:#000000">
							[	
								<xsl:text> </xsl:text>
								<xsl:value-of select="Log/Message[4]/@Date"/> <xsl:text> </xsl:text>
								<xsl:value-of select="Log/Message[4]/@Time"/>
							]
						</text>
                    </td>
                    <td>
                        <text Style="font-family:Segoe UI; color:#24913C">
                            <xsl:value-of select="Log/Message[4]/From/User/@FriendlyName"/>
						</text>	
                    </td>
					<td> 
						: 
					</td>
                    <td>
                        <text Style="font-family:Segoe UI; color:#24913C">
                            <xsl:value-of select="Log/Message[4]/Text"/>
                        </text>		
                    </td>
                </tr>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>



