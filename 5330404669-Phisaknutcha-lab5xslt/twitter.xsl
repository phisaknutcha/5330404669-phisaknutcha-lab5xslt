<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template match="/">

        <html>
			<head>
			    <title>twitter.html</title>            
            </head>
            <body>
                <h2>
                    Twitter @
                    <a href="{rss/channel/link}">
                        <xsl:value-of select="rss/channel/link"/>
                    </a>
                </h2>
                <table border="1">
                    <tr>
                        <th width = "30">Title</th>
                        <th width = "20">Publication Date</th>
                    </tr>
                    <xsl:for-each select="rss/channel/item">
                        <tr>
                            <td>
                                <a href="{rss/channel/item/link}">
                                    <xsl:value-of select="title" />
                                </a>
                            </td>
                            <td>
                                <xsl:value-of select="pubDate" />
                            </td>        
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
 
    </xsl:template>
</xsl:stylesheet>

